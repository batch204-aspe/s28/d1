// CRUD Operations

// Insert Documents (Create)
/*

	SYntax:
		Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			})

		Insert Many Document
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},

				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}

			])



*/

// Insert One 
db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
})

// Insert Many

db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "none"
		},

		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilarmstrong@mail.com",
			"department": "none"
		}
	])


db.courses.insertMany([
		{
			"name": "JavaScript 101",
			"price": 5000,
			"description": "Introduction to JavaScript",
			"isActive": true
		},

		{
			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true
		},

		{
			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": false
		},
	])


// Find Documents or Retrieving Documents (Read)
/*

	db.collectionName.find() - this will retrieve all our documents
	db.collectionName.find({ "criteria": "value" }) - this will retrieve all our documents that will match with our criteria

	db.collectionName.findOne() - this will return the first document in our collection
	
	db.collectionName.findOne({ "criteria": "value" })  - this will return the first document in our collection that will match our criteria

*/
// This will return all document in users collection
db.users.find();


// this will return in users collection of all documents that contain the firstName: Jane
db.users.find({
	"firstName": "Jane"
});

// this will return in users collection of the 1st document
db.users.findOne();

// this will return in users collection of the 1st document that contains the department: none value
db.users.findOne({
	"department": "none"
});


// Update
/*
	
	Syntax:
		// Update One
		db.collectionName.updateOne(
			{
				"criteria": "value"
			},

			{
				$set: {
					"fieldToBeUpdate": "updatedValue"
				}
			}
		)
		
		// Update Multpiple Docment using updateMany
		db.collectionName.updateMany(
			// update the 1st document
			{
				"criteria": "value"
			},

			{
				$set: {
					"fieldToBeUpdate": "updatedValue"
				}
			}
		)
*/

db.users.insertOne(
	{
		"firstName": "Test",
		"lastName": "Test",
		"age": 0,
		"email": "Test@mail.com",
		"department": "none"
	}
)

// Updating One Document
db.users.updateOne(
	{
		"firstName": "Test"
	},

	{
		$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "Operations",
			"status": "active"
		}
	}
)

// Updating Multiple Document
db.users.updateMany(
	{
		"department": "none"
	},

	{
		$set: {
			
			"department": "HR",
		}
	}
);


db.users.updateOne(
		{
			"firstName": "Jane",
			"lastName": "Doe"
		},

		{
			$set:{
				"department": "Operations"
			}
		}
	);


// Removing a field using the $unset: { } method
db.users.updateOne(
		{
			"firstName": "Bill"
		},

		{
			$unset: {
				"status": "active"
			}
		}
	);


db.users.updateMany(
		{
			// leave it as a blank if we want to update or renamed all the fields of department
		},

		{
			$rename: {
				"department": "dept"
			}
		}

	);


/*
	Mini Activity: 
			1. In our courses collection, update the HTML 101 course
				- Make the isActive to false
			2. Add enrollees field to all the documents in our courses collection
				-Enrollees: 10
*/
// Solution for Mini Activity
		// number 1
		db.courses.updateOne(
				{
					"name": "HTML 101"
				},

				{
					$set: {
						"isActive": false
					}
				}
			);

		// number 2
		db.courses.updateMany(
				{}, // updating all the fields of db.collectionName by adding the field of "enrollees"

				{
					$set:{
						"enrollees": 10
					}
				}

			);

// Deleting Documents (DELETE)
/*
	SYNTAX:
		// if there is no criteria & value it will delete the 1st value that it can find

		db.collectionName.deleteOne(
			{
				"criteria" : "value"
			}
		)

		db.collectionName.deleteMany(
			{
				"criteria" : "value"
			}
		)


	
*/

db.users.insertOne({
	"firstName": "Test"

});

// it will delete all the fields that contains the HR dept
db.users.deleteMany(
		{
			"dept": "HR"
		}
	);

// it will delete all the documents of the collectionsName
db.courses.deleteMany({});